defmodule CodeStatsWeb.ProfileLive.DataProviders.DayInfo do
  import CodeStats.Utils.TypedStruct

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  defmodule Data do
    deftypedstruct(%{
      average: number() | nil,
      top_day: %{date: Date.t(), xp: integer()} | nil,
      most_focused: %{date: Date.t(), flow_time: non_neg_integer()} | nil
    })
  end

  @impl true
  def required_data(), do: MapSet.new([:cache])

  @impl true
  @spec retrieve(CodeStatsWeb.ProfileLive.DataProviders.SharedData.t(), CodeStats.User.t()) ::
          Data.t()
  def retrieve(shared_data, _user) do
    most_focused = get_most_focused(shared_data.cache.flows)
    top_day = get_top_day(shared_data.cache.dates)
    average = get_average(shared_data.cache.dates)

    %Data{
      average: average,
      top_day: top_day,
      most_focused: most_focused
    }
  end

  @impl true
  @spec update(Data.t(), CodeStats.User.t(), CodeStats.User.Pulse.t(), CodeStats.User.Cache.t()) ::
          Data.t()
  def update(data, _user, _pulse, cache) do
    # Only most recent flow can be updated live
    most_recent_flow = List.last(cache.flows)

    most_focused =
      if is_nil(most_recent_flow) do
        data.most_focused
      else
        {start_day, next_day} = divide_flow_minutes(most_recent_flow)
        old_most_focused = data.most_focused || %{date: ~D[1970-01-01], flow_time: 0}

        Enum.max_by([old_most_focused, start_day, next_day], & &1.flow_time)
      end

    average = get_average(cache.dates)
    top_day = get_top_day(cache.dates)

    %Data{
      most_focused: most_focused,
      average: average,
      top_day: top_day
    }
  end

  @spec get_most_focused([CodeStats.User.Flow.t()]) ::
          %{date: Date.t(), flow_time: pos_integer()} | nil
  defp get_most_focused(flows) do
    flows
    |> Enum.reduce(%{}, fn flow, acc ->
      {start_day, next_day} = divide_flow_minutes(flow)

      acc
      |> Map.update(start_day.date, start_day.flow_time, &(&1 + start_day.flow_time))
      |> Map.update(next_day.date, next_day.flow_time, &(&1 + next_day.flow_time))
    end)
    |> Enum.max_by(&elem(&1, 1), fn -> nil end)
    |> then(fn
      {date, mins} -> %{date: date, flow_time: mins}
      nil -> nil
    end)
  end

  @spec get_top_day(CodeStats.User.Cache.dates_t()) :: %{date: Date.t(), xp: integer()} | nil
  defp get_top_day(days) do
    days
    |> Enum.max_by(&elem(&1, 1), fn -> nil end)
    |> then(fn
      {date, xp} -> %{date: date, xp: xp}
      nil -> nil
    end)
  end

  @spec get_average(CodeStats.User.Cache.dates_t()) :: number() | nil
  defp get_average(days) do
    case map_size(days) do
      0 -> nil
      size -> days |> Map.values() |> Enum.sum() |> then(&(&1 / size))
    end
  end

  @typep flow_day_data() :: %{date: Date.t(), flow_time: non_neg_integer()}

  @spec divide_flow_minutes(Flow.t()) :: {flow_day_data(), flow_day_data()}
  defp divide_flow_minutes(flow) do
    start_day = NaiveDateTime.to_date(flow.start_time_local)
    next_day = Date.add(start_day, 1)
    next_start_of_day = NaiveDateTime.new!(next_day, ~T[00:00:00])
    diff_to_next_day = NaiveDateTime.diff(next_start_of_day, flow.start_time_local)
    minutes_start_day = min(div(diff_to_next_day, 60), flow.duration)
    minutes_next_day = max(flow.duration - minutes_start_day, 0)

    {
      %{date: start_day, flow_time: minutes_start_day},
      %{date: next_day, flow_time: minutes_next_day}
    }
  end
end
