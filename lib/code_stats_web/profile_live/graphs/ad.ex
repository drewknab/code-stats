defmodule CodeStatsWeb.ProfileLive.Graphs.Ad do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.Graphs
  alias Phoenix.LiveView

  @behaviour Graphs.Behaviour

  @impl Graphs.Behaviour
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([])

  @impl Graphs.Behaviour
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <%= if @show_ads? do %>
      <%= live_component(__MODULE__) %>
    <% else %>
      <div class="no-data"></div>
    <% end %>
    """
  end

  @impl Phoenix.LiveComponent
  def render(assigns) do
    ~H"""
    <div id="profile-ad" phx-hook="AdHook" phx-update="ignore">
      <CodeStatsWeb.Components.Ad.image placement_id="profile-page" dark={false} keywords={[]} />
    </div>
    """
  end
end
