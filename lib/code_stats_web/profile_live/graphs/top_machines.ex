defmodule CodeStatsWeb.ProfileLive.Graphs.TopMachines do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStatsWeb.ProfileLive.Graphs
  alias CodeStatsWeb.ProfileLive.Components
  alias Phoenix.LiveView

  @behaviour Graphs.Behaviour

  @impl true
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([DataProviders.TopMachines])

  @impl true
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <%=
      live_component(
        __MODULE__,
        id: :top_machines_graph,
        top_machines: assigns[DataProviders.TopMachines].top_machines
      )
    %>
    """
  end
end
