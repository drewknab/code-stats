defmodule CodeStats.Utils do
  @moduledoc """
  Contains non-Phoenix-specific utilities that don't fit anywhere else.
  """

  @doc """
  Get configuration setting.

  Uses Application.get_env to get the given setting's value.
  """
  @spec get_conf(atom) :: any
  def get_conf(key) do
    Application.get_env(:code_stats, key)
  end

  @doc """
  Convert integer keys of a map to strings.
  """
  @spec int_keys_to_str(%{optional(integer) => any}) :: %{optional(String.t()) => any}
  def int_keys_to_str(map) do
    Map.new(map, fn {key, value} -> {Integer.to_string(key), value} end)
  end

  @doc """
  Convert string keys of a map to integers.

  If set as strict, will raise if key cannot be parsed as integer, otherwise filters such keys
  out.
  """
  @spec str_keys_to_int(%{optional(String.t()) => any}, boolean()) :: %{optional(integer) => any}
  def str_keys_to_int(map, strict \\ false) do
    map
    |> Enum.map(fn {key, value} ->
      case {Integer.parse(key), strict} do
        {{n, _}, _} -> {n, value}
        {:error, false} -> :invalid
        {:error, true} -> raise "Unable to convert value #{inspect(key)} to integer"
      end
    end)
    |> Enum.filter(&(&1 != :invalid))
    |> Map.new()
  end
end
